class Character(object):

    VOICE = None
    NAME = ["", ""]

    @classmethod
    def get_first_name(cls):
        return cls.NAME[0]

    @classmethod
    def get_last_name(cls):
        return cls.NAME[1]

    @classmethod
    def get_voice(cls):
        return cls.VOICE


class MichaelScott(Character):

    VOICE = "Ted"
    NAME = ["Michael", "Scott"]

class DwightSchrute(Character):

    pass

class JimHalpert(Character):

    pass

class PamBeesly(Character):

    pass

class RyanHoward(Character):

    pass

class AndyBernard(Character):

    pass


class StanleyHudson(Character):

    pass

class KevinMalone(Character):

    pass

class MeredithPalmer(Character):

    pass

class AngelaMartin(Character):

    pass

class OscarMartinez(Character):

    pass

class PhylisVance(Character):

    pass

class TobyFlenderson(Character):

    pass

class KellyKapoor(Character):

    pass

class CreedBratton(Character):

    pass

class DarrylPhilbin(Character):

    pass