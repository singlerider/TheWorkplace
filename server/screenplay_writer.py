import os
import openai
import random

import characters as characters_module

SHOW = "The Office"

characters = [
    character for character in dir(characters_module) 
    if not character.startswith("_") and not character == "Character"
]

openai.api_key = os.getenv("OPENAI_API_KEY")


def prompt_for_characters(number_of_characters):
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt=(
            f"Give me a list of {number_of_characters} "
            f"random recurring characters from {SHOW}."
        ),
        temperature=0.7,
        max_tokens=256,
        top_p=1,
        frequency_penalty=0.4,
        presence_penalty=0.4
    )
    return response


def prompt_for_scenario(scenario_characters):
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt=(
            "Come up with a random scenario involving the following characters"
            f"from {SHOW}:\n{', '.join(scenario_characters)}"
        ),
        temperature=0.7,
        max_tokens=256,
        top_p=1,
        frequency_penalty=0.4,
        presence_penalty=0.4
    )
    return response


def prompt_for_script(scenario_characters, scenario):
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt=(
            f"I would like an original properly-formatted "
            "screenplay for {SHOW} involving the following characters:\n"
            f"{', '.join(scenario_characters)}.\n"
            f"The screnario is as follows:\n{scenario}"
        ),
        temperature=0.7,
        max_tokens=3200,
        top_p=1,
        frequency_penalty=0.4,
        presence_penalty=0.4
    )
    return response


if __name__ == "__main__":
    number_of_characters = random.randint(2, 10)
    scenario_characters = [
        character.split(". ")[1] for character
        in prompt_for_characters(number_of_characters)["choices"][0]["text"].strip().split("\n")
    ]
    print(scenario_characters)
    scenario = prompt_for_scenario(scenario_characters)["choices"][0]["text"]
    print(scenario)
    screenplay = prompt_for_script(scenario_characters, scenario)["choices"][0]["text"]
    print(screenplay)